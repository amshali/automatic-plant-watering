# Automatic plant watering system

This is a super simple automatic plant watering system. I have a plant in my office which I forget to water from time to time or when I am on vacation(because I cannot water it!). 

There are many methods and approaches to automatic gardening and plant watering systems and this is yet another one! However, for whatever reason, you might find this one interesting for your purposes.

## Required hardware
- One photon particle: https://www.particle.io/prototype#photon
- Micro USB cable for photon
- A water pump like this one: https://amzn.com/B010LY7P3Y
- A relay switch: http://www.newegg.com/Product/Product.aspx?Item=9SIA4SR1T53058
- A transistor: [NPN S8050](http://electronics.se-ed.com/magic/s8050.pdf)
- A diode: 1N4007
- [Vinyl tubing](http://www.homedepot.com/p/Sioux-Chief-1-5-8-in-O-D-x-1-1-4-in-I-D-x-2-ft-Clear-Vinyl-Tubing-HSVSP2/204407884)
- A 12 Volt 2 Amp Power Adapter: https://amzn.com/B00Q2E5IXW
- A bread board
- Wires(preferebly with gauge of 22)
- Water resistent tape


## Circuit Wiring
Image below shows the wiring of the system. In this circuit schematic I have used an LED to represent the water pump. In the actual circuit just replace that with the water pump. *IMPORTANT: the water pump is polar. Make sure you are connecting the positive and negative of the water pump to the right wires.*

![Circuit](/circuit-stop.gif?raw=true "")

_(Circuit is drawn using http://www.falstad.com/circuit/)_

### Role of Diode
The role of diode here is to make sure that all the current in the circuit is consumed and dissipated in the form of heat when we switch off the transistor. Otherwise the built-up static electricity behind the transistor could potentially harm the circuit.

## Compile and deploy
### Prerequisites 
1. In order to compile and deploy to your photon device make sure your device is connected using the USB cable to your computer and is ready to be flahsed. Follow [DFU mode](https://docs.particle.io/guide/getting-started/modes/photon/#dfu-mode-device-firmware-upgrade-) on how to put your device in the proper mode. 

1. In addition you would need [particle-cli](https://github.com/spark/particle-cli) command line tools for this.

Now run these commands to compile and deploy the software:
```sh
# Assuming that photon is connected to the computer via USB and
# is on the DFU mode.
particle update
# Assuming you have the code inside 'automatic-plant-watering' dir.
git clone https://github.com/amshali/automatic-plant-watering
particle compile photon automatic-plant-watering --saveTo automatic-plant-watering/watering.bin
particle flash --usb automatic-plant-watering/watering.bin
```

Here is the system in action:
![System in action](/system-in-action.jpg?raw=true "")


## Known Issues
- There is no time setting for watering.  As soon as you power on the system it will start watering and it will do that every 24 hours thereafter.
