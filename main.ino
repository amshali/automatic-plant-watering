#include "application.h"

// Disabling the wifi.
SYSTEM_MODE(MANUAL);

int transistor_base = D1;

// Number of milliseconds in a day.
const unsigned long DAY_MILLIS = 86400000;
// 3 seconds pf watering. Adjust this based on your needs.
const unsigned long WATERING_TIME = 3000;

void setup() {
  pinMode(transistor_base, OUTPUT);
}

void loop() {
  digitalWrite(transistor_base, HIGH);
  delay(WATERING_TIME);
  digitalWrite(transistor_base, LOW);
  delay(DAY_MILLIS - WATERING_TIME);
}

